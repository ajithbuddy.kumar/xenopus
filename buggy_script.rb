require 'thor'

class Calculator < Thor
  desc "add NUM1 NUM2", "Add two numbers"
  def add(num1, num2)
    puts num1.to_f + num2.to_f
  end

  desc "subtract NUM1 NUM2", "Subtract two numbers"
  def subtract(num1, num2)
    puts num1.to_f - num2.to_f
  end

  desc "multiply NUM1 NUM2", "Multiply two numbers"
  def multiply(num1, num2)
    puts num1.to_f * num2.to_f
  end

  desc "divide NUM1 NUM2", "Divide two numbers"
  def divide(num1, num2)
    if num2.to_f == 0
      puts "Cannot divide by zero"
    else
      puts num1.to_f / num2.to_f
    end
  end
end

Calculator.start(ARGV)
