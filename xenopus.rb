require 'dotenv'
Dotenv.load("variables.env")
require "openai"
require 'open3'
require 'json'
require 'colorize'
require 'diffy'
require 'pry'

def cprint(*args, **kwargs)
  text = args.join(' ')
  color = kwargs[:color] || 'white'
  on_color = kwargs[:on_color] || nil
  attrs = kwargs[:attrs] || []

  if on_color
    puts text.colorize(color.to_sym).send("on_#{on_color.to_sym}").send(*attrs)
  else
    puts text.colorize(color.to_sym)
  end
end

 def run_script(script_name, *args)
   begin
     stdout = Open3.capture3("#{RbConfig.ruby} #{script_name} #{args.join(' ')}")
     status = Open3.capture3("#{RbConfig.ruby} #{script_name} #{args.join(' ')}")
     result = stdout
   rescue => e
     return e.to_s, e.status.exitstatus
   end
   return result, 0
 end

 def send_error_to_gpt4(file_path, args, error_message)
   file_lines = File.readlines(file_path)
   file_with_lines = file_lines.map.with_index { |line, i| "#{i + 1}: #{line}" }.join
   initial_prompt_text = File.read("prompt.txt")

   prompt = "#{initial_prompt_text}\n\nHere is the script that needs fixing:\n\n#{file_with_lines}\n\nHere are the arguments it was provided:\n\n#{args}\n\nHere is the error message:\n\n#{error_message}\nPlease provide your suggested changes, and remember to stick to the exact format as described above."
   client = OpenAI::Client.new(access_token: ENV.fetch('OPENAI_ACCESS_TOKEN'))

   response = client.completions(
       parameters: {
           model: "text-davinci-002",
           prompt: prompt,
           max_tokens: 1000
       })

   return response.dig("choices").map { |c| c["text"] }[0]
 end



def apply_changes(file_path, changes_json)
  original_file_lines = File.readlines(file_path)
  changes = JSON.parse(changes_json)

  # Filter out explanation elements
  operation_changes = changes.select { |change| change.key?("operation") }
  explanations = changes.select { |change| change.key?("explanation") }.map { |change| change["explanation"] }

  # Sort the changes in reverse line order
  operation_changes.sort_by! { |change| change["line"] }.reverse!

  file_lines = original_file_lines.dup
  operation_changes.each do |change|
    operation = change["operation"]
    line = change["line"]
    content = change["content"]

    if operation == "Replace"
      file_lines[line - 1] = content + "\n"
    elsif operation == "Delete"
      file_lines.delete_at(line - 1)
    elsif operation == "InsertAfter"
      file_lines.insert(line, content + "\n")
    end
  end

  File.write(file_path, file_lines.join)

  # Print explanations
  cprint("Explanations:", color: :blue)
  explanations.each { |explanation| cprint("- #{explanation}", color: :blue) }

  # Show the diff
  puts "\nChanges:"
  puts Diffy::Diff.new(original_file_lines.join, file_lines.join, context: 3).to_s(:color)
end

def main
  if ARGV.length < 2
    puts "Usage: xenopus.rb <script_name> <arg1> <arg2> ... [--revert]"
    exit 1
  end

  script_name = ARGV[0]
  args = ARGV[1..-1]

  # Revert changes if requested
  if args.include?("--revert")
    backup_file = script_name + ".bak"
    if File.exists?(backup_file)
      FileUtils.cp(backup_file, script_name)
      puts "Reverted changes to #{script_name}"
      exit 0
    else
      puts "No backup file found for #{script_name}"
      exit 1
    end
  end

  # Make a backup of the original script
  FileUtils.cp(script_name, script_name + ".bak")

  loop do
    p run_script(script_name, *args)[1]
    output, status = run_script(script_name, *args)
    if output[2].success?
      puts "Script ran successfully."
      puts "Output: #{output}"
      break
    else
      puts "Script crashed. Trying to fix..."
      puts "Output: #{output}"


      json_response = send_error_to_gpt4(script_name, args, output[1])
      apply_changes(script_name, json_response)
      puts "Changes applied. Rerunning..."
    end
  end
end

main()
