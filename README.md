# Xenopus

## About

Give your Ruby scripts regenerative healing abilities!

Run your scripts with Xenopus and when they crash, GPT edits them and explains what went wrong. Even if you have many bugs it will repeatedly rerun until it's fixed.

## Setup

  bundle install

Add your openAI api key to `variables.env`

## Example Usage

  ruby xenopus.rb buggy_script.rb add 120 20

## Future Plans

This is just a quick prototype I threw together in a few hours. There are many possible extensions and contributions are welcome:

- add flags to customize usage, such as using GPT3.5-turbo instead or asking for user confirmation before running changed code
- further iterations on the edit format that GPT responds in. Currently it struggles a bit with indentation, but I'm sure that can be improved
- a suite of example buggy files that we can test prompts on to ensure reliablity and measure improvement
- multiple files / codebases: send GPT everything that appears in the stacktrace
- graceful handling of large files - should we just send GPT relevant classes / functions?
- extension to languages other than ruby
